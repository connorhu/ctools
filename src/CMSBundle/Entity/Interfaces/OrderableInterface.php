<?php

namespace CT\CMSBundle\Entity\Interfaces;

use \DateTime;

use Doctrine\ORM\Mapping as ORM;

interface OrderableInterface
{
    public function setRecordOrder($value);
    public function getRecordOrder();
}