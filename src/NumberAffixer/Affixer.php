<?php

namespace CT\NumberAffixer;

class Affixer
{
    public static function dateAffixOnDay(\DateTime $dateObject)
    {
        $day = $dateObject->format('d');

        switch ($day)
        {
            case 1:
            case 4:
            case 5:
            case 7:
            case 9:
            case 10:
            case 11:
            case 12:
            case 14:
            case 15:
            case 17:
            case 19:
            case 21:
            case 22:
            case 24:
            case 25:
            case 27:
            case 29:
            case 31:
                return 'én';
        }
    
        return 'án';
    }

    public static function dateAffixOnThatDay(\DateTime $dateObject)
    {
        $day = $dateObject->format('d');

        switch ($day)
        {
            case 1:
            case 4:
            case 5:
            case 7:
            case 9:
            case 10:
            case 11:
            case 12:
            case 14:
            case 15:
            case 17:
            case 19:
            case 21:
            case 22:
            case 24:
            case 25:
            case 27:
            case 29:
            case 31:
                return 're';
        }

        return 'ra';
    }
}
