<?php

namespace CT\SimpleMailerBundle\Services;

use Twig\Environment;

class SimpleMailsenderService
{
    private array $templates;
    private array $templateCache;
    private array $defaultFrom;
    private Environment $twig;
    private $mailer;

    public function __construct(Environment $twig, $mailer, array $defaultFrom, array $templates)
    {
        $this->defaultFrom = $defaultFrom;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->templates = $templates;
    }

    protected function getTemplate($templateName)
    {
        if (isset($this->templateCache[$templateName])) {
            return $this->templateCache[$templateName];
        }
        
        return $this->templateCache[$templateName] = $this->twig->load($templateName);
    }

    public function sendMail($template, $variables, $to, $from = false, $bcc = false, $setters = [])
    {
        if (!isset($this->templates[$template])) {
            throw new \Exception('invalid type');
        }
    
        $template = $this->getTemplate($this->templates[$template]);
        $message = $this->mailer->createMessage()
                    ->setSubject($template->renderBlock('subject', $variables))
                    ->setFrom($from !== false ? $from : $this->defaultFrom)
                    ->setTo($to);
        
        if ($bcc !== false) {
            $message->setBcc($bcc);
        }
        
        foreach ($setters as $name => $value) {
            $message->{'set'. ucfirst($name)}($value);
        }
        
        if ($template->hasBlock('textBody', $variables)) {
            $body = $template->renderBlock('textBody', $variables);
            $message->setBody($body, 'text/plain');
        }
        
        $hasText = false;
        
        if ($template->hasBlock('textBody', $variables)) {
            $hasText = true;
            $body = $template->renderBlock('textBody', $variables);
            $message->setBody($body, 'text/plain');
        }
        
        if ($template->hasBlock('htmlBody', $variables)) {
            $body = $template->renderBlock('htmlBody', $variables);
            
            if ($hasText) {
                $message->addPart($body, 'text/html');
            }
            else {
                $message->setBody($body, 'text/html');
            }
        }

        $this->mailer->send($message);
    }
}