<?php

namespace IEBundle\Form\Types;

use Symfony\Component\Form\Extension\Core\Type\CollectionType,
    Symfony\Component\Form\Extension\Core\Type\ChoiceType,
    Symfony\Component\Form\Extension\Core\Type\TextType,
    Symfony\Component\Form\Extension\Core\Type\CheckboxType,
    Symfony\Component\Form\Extension\Core\Type\SubmitType,
    Symfony\Component\OptionsResolver\OptionsResolver,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;

class ExportSelectorType extends BaseType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setRequired(['meta', 'additional']);
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $meta = $options['meta'];
        $additional = $options['additional'];

        $choices = $additional;

        foreach ($meta as $data) {
            if ($data['fieldName'] === false) {
                $prefix = '';
                $label = $data['entity'];
            }
            else {
                $prefix = $data['fieldName'] .'.';
                $label = $data['entity'] .' ('. $data['fieldName'] .')';
            }
        
            $entityChoices = [];
            foreach ($data['fields'] as $fieldName) {
                $entityChoices[$prefix . $fieldName] = $prefix . $fieldName;
            }
        
            $choices[$label] = $entityChoices;
        }

        $builder->add('fields', CollectionType::class, array(
            'label' => false,
            'entry_type' => ChoiceType::class,
            'entry_options' => [
                'label' => false,
                'choices' => $choices,
            ],

            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true,

            'by_reference' => false,
            'prototype' => true,
            'add_item_button_text' => 'Új mező hozzáadása',
        ));

        $builder->add('save_export_set', CheckboxType::class, array(
            'label' => 'Összeállítás mentése',
        ));

        $builder->add('export_set_name', TextType::class, array(
            'label' => 'Összeállítás neve',
        ));

        $builder->add('global', CheckboxType::class, array(
            'label' => 'Mindenki számára elérhető',
        ));

        $builder->add('save', SubmitType::class, [
            'label' => $this->getDocumentName() .'.form.submit.'. strtolower($options['method'])
        ]);
    }

    public function getBlockPrefix()
    {
        return 'export_selector';
    }
}