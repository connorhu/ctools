<?php

namespace CT\BackAccountTool;

class BankAccountTool
{
    public static $giroToName = [
        100 => 'Magyar Államkincstár',
        101 => [
            'short' => 'Budapest Bank Zrt.',
            'long' => 'Budapest Hitel és Fejlesztési Bank Zrt.',
        ],
        103 => 'Magyar Külkereskedelmi Bank Nyrt.',
        104 => [
            'short' => 'K&H Bank Zrt.',
            'long' => 'Kereskedelmi és Hitelbank Zrt.',
        ],
        105 => 'Általános Értékforgalmi Bank Rt.',
        107 => [
            'short' => 'CIB Bank Zrt.',
            'long' => 'CIB Közép-Európai Nemzetközi Bank Zrt.',
        ],
        108 => [
            'short' => 'Citibank Zrt.',
            'long' => 'Citibank Budapest Zrt.',
        ],
        109 => [
            'short' => 'UniCredit Bank Zrt.',
            'long' => 'UniCredit Bank Hungary Zrt.',
        ],
        111 => 'Inter-Európa Bank Zrt.',
        113 => 'Konzumbank Kereskedelmi Bank Rt.',
        114 => 'Hanwha Bank Magyarország Zrt.',
        115 => 'Magyar Takarékszövetkezeti Bank Zrt.',
        116 => [
            'short' => 'Erste Bank Zrt.',
            'long' => 'Erste Bank Hungary Zrt.',
        ],
        117 => [
            'short' => 'OTP Bank Nyrt.',
            'long' => 'Országos Takarékpénztár és Kereskedelmi Bank Nyrt.',
        ],
        119 => 'Postabank és Takarékpénztár Rt.',
        120 => 'Raiffeisen Bank Zrt.',
        121 => 'Westdeutsche Landesbank (Hungária) Zrt.',
        128 => 'Merkantil Bank Zrt.',
        131 => 'BNP Paribas Hungária Bank Zrt.',
        135 => 'KDB Bank (Magyarország) Zrt.',
        136 => 'CaLyon Bank Magyarország Zrt.',
        137 => 'ING Bank (Magyarország) Zrt.',
        138 => 'Rákóczi Regionális Fejlesztési Bank Rt.',
        141 => 'Magyarországi Volksbank Zrt.',
        142 => 'Commerzbank Budapest Zrt.',
        144 => 'Központi Elszámolóház és Értéktár Zrt. (KELER)',
        146 => 'Magyar Fejlesztési Bank Zrt.',
        147 => 'IC Bank Zrt.',
        148 => 'Magyar Export-Import Bank Zrt. (Eximbank)',
        160 => 'Porsche Bank Hungária Zrt.',
        162 => 'HBW Express Takarékszövetkezet',
        163 => 'Deutsche Bank Zrt.',
        164 => 'GMAC Bank Hungary Rt.',
        167 => 'Magyar Cetelem Bank Zrt.',
        168 => 'FHB Földhitel és Jelzálog Bank Nyrt.',
        170 => 'Ella Első Lakáshitel Kereskedelmi Bank Zrt.',
        171 => 'UniCredit Jelzálogbank Zrt.',
        172 => 'Credigen Bank Zrt.',
        173 => 'Polgári Kereskedelmi Bank Rt.',
        174 => 'Dresdner Bank (Hungária) Rt.',
        175 => 'Bank of China Zrt.',
        176 => 'EB und HYPO Bank Burgenland-Sopron Zrt.',
        177 => 'Dresdner Bank AG Magyarországi Fióktelepe',
        178 => 'Fortis Bank SA/NV Magyarországi Fióktelepe',
        179 => 'Bank Plus Bank Zrt.',
        180 => 'Cofidis Magyarországi Fióktelep',
        181 => 'Allianz Bank Zrt.',
        182 => [
            'short' => 'FHB Bank Zrt.',
            'long' => 'FHB Kereskedelmi Bank Zrt.',
        ],
        183 => 'BNP Paribas Magyarországi Fióktelepe',
        184 => 'Oberbank AG Magyarországi Fióktelepe',
        185 => 'Calyon S.A. Magyarország Bankfióktelep',
        190 => 'Magyar Nemzeti Bank',
        503 => 'Mecsek-Vidéke Takarékszövetkezet, Mecseknádasd',
        506 => 'Siklós és Vidéke Takarékszövetkezet, Siklós',
        527 => 'Soltvadkert és Vidéke Takarékszövetkezet, Soltvadkert',
        539 => 'Szarvas és Vidéke Takarékszövetkezet, Szarvas',
        572 => 'Szegvár és Vidéke Takarékszövetkezet, Szegvár',
        603 => 'Hajdúdorog és Vidéke Takarékszövetkezet,',
        619 => 'Füzesabony és Vidéke Takarékszövetkezet, Füzesabony',
        644 => 'Alsónémedi és Vidéke Takarékszövetkezet, Alsónémedi',
        652 => 'Nagykáta és Vidéke Takarékszövetkezet, Nagykáta',
        655 => 'Örkényi Takarékszövetkezet, Örkény',
        657 => 'Pilisvörösvár és Vidéke Takarékszövetkezet, Pilisvörösvár',
        659 => 'Turai Takarékszövetkezet, Tura',
        700 => 'Tiszaföldvár és Vidéke Takarékszövetkezet, Tiszaföldvár',
        803 => 'Tiszántúli Első Hitelszövetkezet',
        807 => 'Általános Közlekedési Hitelszövetkezet',
        808 => 'Széchenyi István Hitelszövetkezet',
        880 => 'Fundamenta Magyar-Német Lakástakarékpénztár Rt.',
        881 => 'OTP Lakástakarékpénztár Rt.',
        882 => 'Lakáskassza Első Általános Lakástakarékpénztár Rt.',
        883 => 'Otthon Magyar-Osztrák Lakástakarékpénztár Rt.',
        880 => 'Fundamenta Lakáskassza Lakástakarékpénztár Zrt.',
        881 => 'OTP Lakástakarékpénztár Zrt.',
        884 => 'OTP Jelzálogbank Zrt.',
    ];

    public static function bankNameWithAccountNumber($accountNumber, $short = true)
    {
        $giro = substr($accountNumber, 0, 3);

        if (!isset(self::$giroToName[$giro])) {
            return null;
        }

        if (!isset(self::$giroToName[$giro]['short'])) {
            return self::$giroToName[$giro];
        }

        if (true === $short && isset(self::$giroToName[$giro]['short'])) {
            return self::$giroToName[$giro]['short'];
        }
        if (false === $short && isset(self::$giroToName[$giro]['long'])) {
            return self::$giroToName[$giro]['long'];
        }

        return null;
    }
}
