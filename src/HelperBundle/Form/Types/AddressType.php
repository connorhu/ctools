<?php

namespace CT\HelperBundle\Form\Types;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormInterface,
    Symfony\Component\Form\FormView,
    Symfony\Component\Form\Extension\Core\Type\TextType,
    Symfony\Component\Form\Extension\Core\Type\HiddenType,
    Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address',
            'optional_visibility' => false,
            'street' => true,
        ));
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('zipCode', TextType::class, [
            'label' => 'Irányítószám',
            'required' => false,
        ]);

        $builder->add('city', HiddenType::class, [
            'empty_data' => '-',
            'required' => false,
        ]);
        
        if ($options['street']) {
            $builder->add('street', TextType::class, [
                'label' => 'Utca',
                'required' => false,
            ]);
        }
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['optional_visibility'] = $options['optional_visibility'];
        $view->vars['optional_button_label'] = 'Cím hozzáadása';
    }
    
    public function getBlockPrefix()
    {
        return 'address';
    }
}