<?php

namespace CT\CMSBundle\Entity\Traits;

use \DateTime;

use Doctrine\ORM\Mapping as ORM;

trait VisibilityTrait
{
    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $public = false;
    
    /**
     * setter for public
     *
     * @param mixed $value
     * @return self
     */
    public function setPublic($value)
    {
        $this->public = $value;
        return $this;
    }
    
    /**
     * getter for public
     * 
     * @return mixed return value for 
     */
    public function getPublic()
    {
        return $this->public;
    }
    
    public function isPublic()
    {
        return $this->public;
    }
}