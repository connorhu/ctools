<?php

namespace CT\HelperBundle\Twig;

use \Michelf\MarkdownExtra;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class MarkdownExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('markdown', array($this, 'markdown')),
        );
    }

    public function markdown($string)
    {
        return MarkdownExtra::defaultTransform($string);
    }

    public function getName()
    {
        return 'connor_markdown';
    }
}