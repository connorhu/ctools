<?php

namespace CT\CMSBundle\Entity\Interfaces;

use \DateTime;

use Doctrine\ORM\Mapping as ORM;

interface VisibilityInterface
{
    const VISIBILITY_PUBLIC = 1;
    const VISIBILITY_PRIVATE = 0;

    public function setPublic($value);
    public function getPublic();
}