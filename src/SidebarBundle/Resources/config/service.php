<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use CT\SidebarBundle\Controller\SidebarController;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

return function(ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services
        ->set(SidebarController::class)
        ->args(['%sidebar_config%'])
        ->autowire()->autoconfigure()
        ->tag('controller.service_arguments')
    ;

};
