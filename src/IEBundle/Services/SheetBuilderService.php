<?php

namespace CT\IEBundle\Services;

use Symfony\Component\HttpFoundation\BinaryFileResponse,
    Symfony\Component\HttpFoundation\ResponseHeaderBag;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use \PHPExcel,
    \PHPExcel_Writer_Excel2007,
    \PHPExcel_IOFactory,
    \PHPExcel_Cell,
    \PHPExcel_Shared_Date;

class SheetBuilderService
{
    private $rootDir;
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }
    
    public function newSheet()
    {
        return new Spreadsheet();
    }
    
    public function downloadFile($doc, $filename, $request)
    {
        $fileInfo = pathinfo($filename);
        
        $writer = null;
        
        switch ($fileInfo['extension']) {
            case 'xlsx':
                $writer = IOFactory::createWriter($doc, 'Xlsx');
                break;
        }
        
        $targetPath = $this->rootDir .'/'. $filename;
        
        $writer->save($targetPath);
        $response = new BinaryFileResponse($targetPath);
        $disp = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
        $response->headers->set('Content-Disposition', $disp);
        $response->deleteFileAfterSend(true);
        
        return $response;
    }
    
    public function getFileContent($filepath)
    {
        $reader = IOFactory::createReaderForFile($filepath);
        $doc = $reader->load($filepath);
        
        $rows = [];
        foreach ($doc->getAllSheets() as $sheet) {
            $highest = $sheet->getHighestRowAndColumn();
            $highest['column'] = PHPExcel_Cell::columnIndexFromString($highest['column']);
            
            for ($row = 1; $row <= $highest['row']; $row++) {
                $cols = [];
                for ($col=0; $col < $highest['column']; $col++) {
                    $cell = $sheet->getCellByColumnAndRow($col, $row);
                    
                    if (PHPExcel_Shared_Date::isDateTime($cell)) {
                        $cols[] = PHPExcel_Shared_Date::ExcelToPHPObject($cell->getValue());
                    }
                    else {
                        $cols[] = $sheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
                    }
                }

                $hasContent = false;
                foreach ($cols as $cell) {
                    if (strlen($cell) > 0) {
                        $hasContent = true;
                        break;
                    }
                }

                if ($hasContent === false) {
                    continue;
                }

                $rows[] = $cols;
            }
        }
        
        return $rows;
    }
}