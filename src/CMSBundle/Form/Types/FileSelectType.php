<?php

namespace CT\CMSBundle\Form\Types;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormInterface,
    Symfony\Component\Form\FormView,
    Symfony\Component\Form\Extension\Core\Type\FileType,
    Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\Common\Persistence\ObjectManager;

use KMC\EventPlannerBundle\Entity\Media;

class FileSelectType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'multiple' => false,
            'library' => false,
        ]);
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('upload', FileType::class, [
                'mapped' => false,
                'label' => false,
                'required'  => false,
            ]);
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multiple'] = $options['multiple'];
        $view->vars['media_library'] = $options['library'];
    }
    
    public function getBlockPrefix(): string
    {
        return 'file_select';
    }
}
