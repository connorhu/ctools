<?php

namespace CT\CMSBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs,
    Doctrine\ORM\Event\PreUpdateEventArgs;

use CT\CMSBundle\Entity\Interfaces\TimestampableInterface;
use CT\CMSBundle\Entity\Interfaces\OrderableInterface;

class TimestampableDoctrineListener
{
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $document = $args->getEntity();
        
        if ($document instanceof TimestampableInterface) {
            $document->updateFieldUpdatedAt();
        }
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $document = $args->getObject();
        
        if ($document instanceof TimestampableInterface) {
            $document->updateFieldCreatedAt();
            $document->updateFieldUpdatedAt();
        }
    }
}