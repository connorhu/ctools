<?php

namespace CT\HelperBundle\Controller;

use App\Entity\ZipCode;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AddressController extends AbstractController
{
    /**
     * @Route("/fetch", name="fetch_city")
     */
    public function fetchCityAction(Request $request)
    {
        $zipCode = $request->get('zip', false);
        if (!$zipCode) {
            throw $this->createNotFoundException();
        }
        
        $result = $this->getDoctrine()->getRepository(ZipCode::class)->findOneByCode(['code' => (int) $zipCode]);
        
        $city = $part = false;
        if ($result) {
            $city = $result['township'];
            if (isset($result['part'])) {
                $part = $result['part'];
            }
        }
        else {
            throw $this->createNotFoundException();
        }
        
        $response = new JsonResponse();
        $response->setData(array(
            'city' => $city,
            'part' => $part
        ));
        return $response;
    }
    
    /**
     * @Route("/country/fetch", name="fetch_country")
     */
    public function fetchCountryAction()
    {
        $result = $this->getDoctrine()->getRepository('KMCEventPlannerBundle:Country')->findBy([], ['preferred' => 'DESC', 'name' => 'ASC']);
        $countries = [];
        
        foreach ($result as $row) {
            $countries[] = $row->toArray();
        }
        
        $response = new JsonResponse();
        $response->setData(array(
            'countries' => $countries,
        ));
        return $response;
    }
}
