<?php

namespace CT\CMSBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CommonExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return array(
            new TwigFilter('is_date', array($this, 'is_date')),
        );
    }

    public function is_date($value)
    {
        return is_object($value) && $value instanceof \DateTime;
    }

    public function getName(): string
    {
        return 'connor_common';
    }
}