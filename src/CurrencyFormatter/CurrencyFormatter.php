<?php

namespace CT\CurrencyFormatter;

use \NumberFormatter;

class CurrencyFormatter
{
    private static function initFormatter($currency)
    {
        if ($currency === 'HUF') {
            $formatter = new NumberFormatter('hu_HU', NumberFormatter::CURRENCY);
            $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
            $formatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, 'Ft');
        }
        elseif ($currency === 'EUR') {
            $formatter = new NumberFormatter('eu_EU', NumberFormatter::CURRENCY);
            $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
        }
        return $formatter;
    }

    public static function format($number, $currency = 'HUF')
    {
        static $formatters = [];
        
        if (!isset($formatters[$currency])) {
            $formatters[$currency] = self::initFormatter($currency);
        }
        
        return $formatters[$currency]->format($number);
    }
}