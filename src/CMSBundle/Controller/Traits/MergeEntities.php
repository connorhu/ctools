<?php

namespace CT\CMSBundle\Controller\Traits;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Mapping\ClassMetadata;

use App\EntityFilter\FieldFinder\FieldFinder,
    App\EntityFilter\Services\FilterFormBuilder,
    App\EntityFilter\Helpers\QueryBuilderHelper,
    App\Merge\Form\FormBuilderHelper,
    App\Merge\Services\MetadataReader;

trait MergeEntities
{
    public function handleSelectMerge()
    {
        $documents = $this->getDocuments($this->request->query->get('ids'));
        $metadataReader = new MetadataReader();
        $metadata = $metadataReader->readMetadata($documents[0]);

        $form = $this->getMergeForm($metadata, $documents)->getForm();

        if (count($form) == 0) {
            $this->mergeDocuments($documents, $metadata, $form);
            return $this->routeRedirectView('get_'. $this->documentNames['plural']);
        }

        $data = [
            'form' => $form
        ];

        $templateData = $this->templateData('edit');

        return $this->view($data, 200)
            ->setTemplateData($templateData)
            ->setTemplate($this->templateSubdir .'/'. $this->documentNames['camel'] .'/select_merge.html.twig');
    }
    
    public function handleDoMerge()
    {
        $em = $this->getDoctrine()->getManager();
        $documents = $this->getDocuments($this->request->query->get('ids'));
        $metadataReader = new MetadataReader();
        $metadata = $metadataReader->readMetadata($documents[0]);
        $entityMetadata = $this->getDoctrine()->getManager()->getClassMetadata(get_class($documents[0]));

        $form = $this->getMergeForm($metadata, $documents)->getForm();
        $form->handleRequest($this->request);

        if ($form->isValid()) {
            $this->mergeDocuments($documents, $metadata, $form);
            return $this->routeRedirectView('get_'. $this->documentNames['plural']);
        }
    }


    public function mergeMany($propertyName, $target, $source) {
        $entities = $source->{'get'. ucfirst($propertyName)}();

        if (count($entities) === 0) return;

        $singularProperty = Inflector::singularize($propertyName);

        foreach ($entities as $entity) {
            $source->{'remove'. ucfirst($singularProperty)}($entity);
            $target->{'add'. ucfirst($singularProperty)}($entity);
        }
    }

    protected function getMergeForm($metadata, $documents)
    {
        $formData = FormBuilderHelper::formFields($metadata, $documents);

        $form = $this->createFormBuilder();

        if (count($formData) > 0) {
            foreach ($formData as $data) {
                $form->add($data['name'], $data['type'], $data['args']);
            }
        }
        else {
            return $form;
        }

        $form->add('save', SubmitType::class, array('label' => 'Merge entities'));
        $form->setAction($this->generateUrl('merge_'. $this->documentNames['plural'], $this->request->query->all()));

        return $form;
    }

    protected function mergeDocuments($documents, $metadata, $form)
    {
        $em = $this->getDoctrine()->getManager();
        $entityMetadata = $this->getDoctrine()->getManager()->getClassMetadata(get_class($documents[0]));

        $target = array_shift($documents);
        $targetId = $target->getId();

        $sources = [];
        foreach ($documents as $document) {
            $sources[$document->getId()] = $document;
        }

        foreach ($metadata['properties'] as $property) {
            $name = $property['name'];

            if ($property['annotation'] && $property['annotation']->disabled) continue;

            $isHidden = $property['annotation'] && $property['annotation']->hidden === true;
            if (!isset($form[$name]) && $isHidden === false) continue;

            if ($entityMetadata->hasField($name)) {
                if ($isHidden) continue;

                $id = $form[$name]->getData();

                if (!$id) continue;
                if ($id == $targetId) continue;

                $mapping = $entityMetadata->getFieldMapping($name);

                switch ($mapping['type']) {
                    case 'string':
                    case 'text':
                    case 'integer':
                    case 'boolean':
                        $target->{'set'. ucfirst($name)}($sources[$id]->{'get'. ucfirst($name)}());
                        break;
                    default:
                        var_dump($mapping['type']);
                        exit;
                        break;
                }
            }
            elseif ($entityMetadata->hasAssociation($name)) {
                $mapping = $entityMetadata->getAssociationMapping($name);

                switch ($mapping['type']) {
                    case ClassMetadata::ONE_TO_MANY:
                    case ClassMetadata::MANY_TO_MANY:
                        foreach ($documents as $document) {
                            $this->mergeMany($name, $target, $document);
                        }
                        break;

                    case ClassMetadata::MANY_TO_ONE:

                        if (!isset($sources[$id])) {
                            $value = null;
                        }
                        else {
                            $value = $sources[$id]->{'get'. ucfirst($name)}();
                        }

                        $target->{'set'. ucfirst($name)}($value);
                        break;

                    default:
                        var_dump($mapping['type']);
                        exit;
                        break;
                }
            }
        }

        foreach ($documents as $document) {
            $em->remove($document);
        }

        $em->flush();
    }
}