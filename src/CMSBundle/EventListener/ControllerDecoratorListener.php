<?php

namespace CT\CMSBundle\EventListener;

use Symfony\Component\HttpKernel\Event\ControllerEvent;

class ControllerDecoratorListener
{
	public function onKernelController(ControllerEvent $event)
	{
        $controller = null;

        if (is_array($event->getController())) {
            $controller = $event->getController()[0];
        }

        if (is_object($event->getController())) {
            $controller = $event->getController();
        }

        if ($controller !== null && in_array('CT\CMSBundle\Controller\RequestAwareInterfase', class_implements($controller)) === true) {
            $controller->request = $event->getRequest();
        }
	}
}