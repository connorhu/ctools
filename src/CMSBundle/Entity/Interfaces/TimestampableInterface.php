<?php

namespace CT\CMSBundle\Entity\Interfaces;

use \DateTime;

use Doctrine\ORM\Mapping as ORM;

interface TimestampableInterface
{
    public function updateFieldUpdatedAt();
    public function updateFieldCreatedAt();

    public function getCreatedAt();
    public function getUpdatedAt();
}