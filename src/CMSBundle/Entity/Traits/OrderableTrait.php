<?php

namespace CT\CMSBundle\Entity\Traits;

use \DateTime;

use Doctrine\ORM\Mapping as ORM;

trait OrderableTrait
{
    /**
     * @ORM\Column(name="recordOrder", type="integer", nullable=true)
     */
    protected $recordOrder;

    /**
     * setter for recordOrder
     *
     * @param mixed $value
     * @return self
     */
    public function setRecordOrder($value)
    {
        $this->recordOrder = $value;
        return $this;
    }
    
    /**
     * getter for recordOrder
     * 
     * @return mixed return value for 
     */
    public function getRecordOrder()
    {
        return $this->recordOrder;
    }
    
    /**
     * setter for recordOrderAfter
     *
     * @param mixed $value
     * @return self
     */
    public function setRecordOrderAfter($insertAfterObject)
    {
        $this->recordOrder = $insertAfterObject->getRecordOrder()+1;
        return $this;
    }

    public function setRecordOrderFirst()
    {
        $this->recordOrder = 1;
        return $this;
    }
    
    public function setRecordOrderEmpty()
    {
        $this->recordOrder = null;
        return $this;
    }
}