<?php

namespace CT\CMSBundle\Form\Types;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormInterface,
    Symfony\Component\Form\FormView,
    Symfony\Component\Form\Extension\Core\Type\TextType,
    Symfony\Component\OptionsResolver\OptionsResolver,
    Symfony\Component\Form\CallbackTransformer,
    Symfony\Component\Validator\Constraints\NotBlank;

use Doctrine\Common\Persistence\ObjectManager;

use KMC\EventPlannerBundle\Entity\Media;

class PhoneType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'compound' => true,
            'prefix_field' => true
        ));
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $prefixMode = $options['prefix_field'];

        if ($prefixMode) {
            $builder->add('prefix', TextType::class, [
                'label' => false,
                'required'  => false,
                'constraints' => [
                    new NotBlank(),
                ]
            ]);
        }

        $builder->add('number', TextType::class, [
            'label' => false,
            'required'  => false,
            'constraints' => [
                new NotBlank(),
            ]
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            function (?string $phoneNumber) use ($prefixMode) {
                $prefix = '';
                $number = '';
                
                if (strpos($phoneNumber, '/') !== false) {
                    list($prefix, $number) = explode('/', $phoneNumber);
                }
                else {
                    $number = $phoneNumber;
                }
                
                return [
                    'prefix' => $prefix,
                    'number' => $number
                ];
            },
            function (array $phoneNumberParts) use ($prefixMode) {
                $prefix = $prefixMode === true ? ($phoneNumberParts['prefix'] .'/') : '';
                return $prefix.$phoneNumberParts['number'];
            }
        ));
    }

    public function getParent(): string
    {
        return TextType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'phone';
    }
}
