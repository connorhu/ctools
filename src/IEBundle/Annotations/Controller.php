<?php

namespace CT\IEBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class Controller
{
    public $name;
    
    public function __construct($name)
    {
        $this->name = $name;
    }
}