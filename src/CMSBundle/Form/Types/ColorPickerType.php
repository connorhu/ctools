<?php

namespace CT\CMSBundle\Form\Types;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormInterface,
    Symfony\Component\Form\FormView,
    Symfony\Component\Form\Extension\Core\Type\TextType,
    Symfony\Component\OptionsResolver\OptionsResolver,
    Symfony\Component\Form\CallbackTransformer;

class ColorPickerType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => [
                'class' => 'color_picker'
            ],
        ));
    }
    
    protected function getColorTransformer()
    {
        static $transformer;
        
        if (!$transformer) {
            $transformer = new CallbackTransformer([$this, 'toFieldData'], [$this, 'toEntityData']);
        }
        
        return $transformer;
    }
    
    public static function toFieldData($value): string
    {
        if (!$value) return '';
        
        return '#'. $value;
    }

    public static function toEntityData($value)
    {
        return substr(str_replace('#', '', $value), 0, 6);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->getColorTransformer());
    }
    
    public function getParent(): string
    {
        return TextType::class;
    }

    public function getName(): string
    {
        return 'color_picker';
    }
}
