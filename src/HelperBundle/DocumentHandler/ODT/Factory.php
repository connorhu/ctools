<?php

namespace CT\HelperBundle\DocumentHandler\ODT;

use Symfony\Component\Filesystem\Filesystem;

class Factory
{
    private $fileLocator;
    private $tempDir;
    
    private $twig;
    
    private $printerName = false;
    
    public function __construct($fileLocator, $homeDir)
    {
        $this->fileLocator = $fileLocator;
        $this->homeDir = $homeDir;
    }
    
    private function determinateLibreOffice()
    {
        $uname = php_uname('o');
        
        if (strpos($uname, 'Darwin') !== false) {
            // 4.4.0
            return '/Applications/LibreOffice.app/Contents/MacOS/soffice';
        }
        elseif (strpos($uname, 'Ubuntu') !== false) {
            return '/usr/bin/libreoffice';
        }
        elseif (strpos($uname, 'Linux') !== false) {
            return '/usr/bin/libreoffice';
        }
        elseif (strpos($uname, 'Windows') !== false) {
            
            $programFiles = null;
            if ($_SERVER['PROCESSOR_ARCHITECTURE'] == 'AMD64') {
                $programFiles = $_SERVER['ProgramFiles(x86)'];
            }
            else {
                $programFiles = $_SERVER['ProgramFiles'];
            }
            
            return "\"". $programFiles ."\\LibreOffice 4\\program\\soffice.exe\"";
        }
        
        throw new \Exception('unknown os type: '. php_uname('s'));
    }

    public function createDocumentInstance($templateDocumentName, $instanceName)
    {
        $fs = new Filesystem();
        $fs->copy($this->fileLocator->locate($templateDocumentName), $this->getTempDir() .'/'. $instanceName);
        
        $document = new Instance($this->getTempDir() .'/'. $instanceName);
        $document->setTemplateEngine($this->twig);
        
        return $document;
    }
    
    public function printDocument(Instance $document)
    {
        if (!$this->printerName) {
            return;
        }
        
        $args = [
            '--invisible',
            '--headless',
            '--convert-to pdf',
            '--outdir "'. dirname($document->getDocumentPath()) .'"',
            '"'. $document->getDocumentPath() .'"'
        ];
        
        $commandToExecute = $this->determinateLibreOffice() .' '. implode(' ', $args);
        
        $descriptorspec = array(
           0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
           1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
           2 => array("pipe", "w") // stderr is a file to write to
        );

        $cwd = '/tmp';
        $env = [
            'HOME' => realpath($this->homeDir),
        ];

        $process = proc_open($commandToExecute, $descriptorspec, $pipes, $cwd, $env);

        if (is_resource($process)) {

            // dump(stream_get_contents($pipes[1]), stream_get_contents($pipes[2]));

            fclose($pipes[1]);
            fclose($pipes[2]);

            // It is important that you close any pipes before calling
            // proc_close in order to avoid a deadlock
            $return_value = proc_close($process);
            
            // dump($return_value);
        }
        
        return $document->getDocumentPDFPath();
    }
    
    /**
     * setter for printerName
     *
     * @param mixed 
     * @return self
     */
    public function setPrinterName($value)
    {
        $this->printerName = $value;
        return $this;
    }
    
    /**
     * getter for printerName
     * 
     * @return mixed return value for 
     */
    public function getPrinterName()
    {
        return $this->printerName;
    }
    
    /**
     * setter for libreofficePath
     *
     * @param mixed 
     * @return self
     */
    public function setLibreofficePath($value)
    {
        $this->libreofficePath = $value;
        return $this;
    }
    
    /**
     * getter for libreofficePath
     * 
     * @return mixed return value for 
     */
    public function getLibreofficePath()
    {
        return $this->libreofficePath;
    }

    /**
     * setter for tempDir
     *
     * @param mixed 
     * @return self
     */
    public function setTempDir($value)
    {
        $this->tempDir = $value;
        return $this;
    }
    
    /**
     * getter for tempDir
     * 
     * @return mixed return value for 
     */
    public function getTempDir()
    {
        return $this->tempDir;
    }
    
    /**
     * setter for twig
     *
     * @param mixed 
     * @return self
     */
    public function setTwig($value)
    {
        $this->twig = $value;
        return $this;
    }
}
