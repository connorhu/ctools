<?php

namespace ConnorToolsBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
// use Symfony\Component\PropertyAccess\PropertyAccess;
// use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class EntityTypeExtension extends AbstractTypeExtension
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'add_item_button_text' => 'Add new',
            'add_item_url' => false
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['add_item_button_text'] = $options['add_item_button_text'];
        $view->vars['add_item_url'] = $options['add_item_url'];
    }
    
    public function getExtendedType(): string
    {
        return 'entity';
    }
}
