<?php

namespace CT\HelperBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class WordCountValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value !== null) {
            $words = explode(' ', $value);
            
            $count = 0;
            foreach ($words as $word) {
                if (strlen($word) > 0) {
                    $count++;
                }
            }
            
            if ($constraint->max && $count > $constraint->max) {
                $this->context->buildViolation($constraint->message)
                                ->setParameter('{{ limit }}', $constraint->max)
                                ->addViolation();
            }
        }
    }
}