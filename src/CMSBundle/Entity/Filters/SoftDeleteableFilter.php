<?php

namespace CT\CMSBundle\Entity\Filters;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable;

class SoftDeleteableFilter extends SQLFilter
{
    private $entityManager;
    
    public function addFilterConstraint(ClassMetadata $metadata, $targetTableAlias)
    {
        if (!in_array(SoftDeletable::class, $metadata->reflClass->getTraitNames())) {
            return '';
        }
        
        $conn = $this->getEntityManager()->getConnection();
        $platform = $conn->getDatabasePlatform();
        $column = $metadata->getQuotedColumnName('deletedAt', $platform);
        $addCondSql = $platform->getIsNullExpression($targetTableAlias.'.'.$column);
        $now = $conn->quote(date($platform->getDateTimeFormatString())); // should use UTC in database and PHP
        $addCondSql = "({$addCondSql} OR {$targetTableAlias}.{$column} > {$now})";

        return $addCondSql;
    }

    protected function getEntityManager()
    {
        if ($this->entityManager === null) {
            $refl = new \ReflectionProperty('Doctrine\ORM\Query\Filter\SQLFilter', 'em');
            $refl->setAccessible(true);
            $this->entityManager = $refl->getValue($this);
        }
        return $this->entityManager;
    }
}