<?php

namespace CT\IEBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Field
{
    public $label;
    
    public function __construct($values)
    {
        $this->label = $values['label'];
    }
}