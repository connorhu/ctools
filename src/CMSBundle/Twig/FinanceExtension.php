<?php

namespace CT\CMSBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FinanceExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return array(
            new TwigFilter('gross', array($this, 'gross'), ['needs_context' => true]),
        );
    }

    public function gross($context, $value, $type)
    {
        if (!isset($context['vat_rates'][$type])) {
            return $value;
        }
        
        if (is_array($value)) {
            $tmp = [];
            foreach ($value as $k => $v) {
                $tmp[$k] = $this->gross($context, $v, $type);
            }
            return $tmp;
        }
        
        return round($value * (($context['vat_rates'][$type]['rate'] / 100) + 1));
    }

    public function getName(): string
    {
        return 'connor_finance';
    }
}