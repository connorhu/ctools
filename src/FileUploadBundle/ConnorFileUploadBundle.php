<?php

namespace Connor\FileUploadBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

use Connor\FileUploadBundle\DependencyInjection\FileUploadExtension;

class ConnorFileUploadBundle extends Bundle
{
    public function getContainerExtension(): string
    {
        return new FileUploadExtension();
    }
}
