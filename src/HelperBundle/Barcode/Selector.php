<?php

namespace CT\HelperBundle\Barcode;

use BG\BarcodeBundle\Util\Base1DBarcode;
use BG\BarcodeBundle\Util\Base2DBarcode;

use Symfony\Component\Filesystem\Filesystem;

class Selector
{
    private $tempDirectory;
    private $publicDirectory;
    private $publicPathPrefix;
    
    private $filesystem;
    
    public function __construct($tempDirectory, $publicDirectory, $publicPathPrefix)
    {
        $this->tempDirectory = $tempDirectory;
        $this->publicDirectory = $publicDirectory;
        $this->publicPathPrefix = $publicPathPrefix;

        $this->filesystem = new Filesystem();
    }
    
    const BARCODE_DIMENSION_D1 = 'D1';
    const BARCODE_DIMENSION_D2 = 'D2';
    
    public function generatorInstance($type = self::BARCODE_DIMENSION_D1)
    {
        switch ($type)
        {
            case self::BARCODE_DIMENSION_D1:
                $instance = new Base1DBarcode();
                break;

            case self::BARCODE_DIMENSION_D2:
                $instance = new Base2DBarcode();
                break;
        }
        
        $instance->savePath = $this->tempDirectory .'/';
        
        return $instance;
    }
    
    public function generate(BarcodeInterface $document, $force = false)
    {
        $path = $this->tempDirectory .'/C128_'. $document->getBarcode() .'.png';
        if ($force || !$this->filesystem->exists($path)) {
            $path = $this->generatorInstance(Selector::BARCODE_DIMENSION_D1)->getBarcodePNGPath($document->getBarcode(), 'C128');
        }
            
        return $path;
    }
    
    public function deploy($privatePath, $force = false)
    {
        $publicPath = $this->publicDirectory .'/'. basename($privatePath);
        
        if ($force || !$this->filesystem->exists($publicPath)) {
            $this->filesystem->copy($privatePath, $publicPath);
        }
        
        return $this->publicPathPrefix .'/'. basename($privatePath);
    }
}