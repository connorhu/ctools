<?php

namespace CT\CMSBundle\Controller\Traits;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Inflector\EnglishInflector;
use function Symfony\Component\String\u;

trait ControllerTrait
{
    protected function createDocumentNames(string $documentName) : array
    {
        $inflector = new EnglishInflector();

        $plural = '';
        if ($documentName === 'conspectus') {
            $plural = 'conspectuses';
        } else {
            $plural = $inflector->pluralize($documentName)[0];
        }

        return [
            'singular' => str_replace(' ', '', $documentName),
            'plural' => str_replace(' ', '', $plural),
            'camel' => str_replace(' ', '', u($documentName)->title(true)),
        ];
    }

    protected function isEdit(?Request $request = null, $routePrefix = '')
    {
        if ($request === null) {
            @trigger_error('isEdit() is deprecated without request argument.', E_USER_DEPRECATED);
            $request = $this->request;
        }
        
        return strpos($request->attributes->get('_route'), $routePrefix.'edit') === 0 || $request->getMethod() === 'PUT' || $request->getMethod() === 'PATCH';
    }
    
    protected function isView(Request $request, $routePrefix = '')
    {
        return strpos($request->attributes->get('_route'), $routePrefix.'view_') === 0;
    }
}