<?php

namespace CT\CMSBundle\Entity;

use CT\CMSBundle\Entity\Interfaces\TimestampableInterface;
use CT\CMSBundle\Entity\Traits\TimestampableTrait;

abstract class BaseEntity implements TimestampableInterface
{
    use TimestampableTrait;
}
