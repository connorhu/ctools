<?php

namespace CT\CMSBundle\Entity\Interfaces;

use \DateTime;

use Doctrine\ORM\Mapping as ORM;

interface GrouppedOrderableInterface
{
    public static function getOrderingGroupName();
}