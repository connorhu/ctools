<?php

namespace CT\HelperBundle\DocumentHandler\ODT;

use DateTime;
use ZipArchive;

class Instance
{
    private $documentPath;
    private $templateEngine;
    
    private $stylesTemplate = null;
    private $contentTemplate = null;
    
    public function __construct($filePath)
    {
        $this->documentPath = $filePath;
    }
    
    public function render($variables = [])
    {
        $zip = new ZipArchive();
        $zip->open($this->documentPath);

        $variables['items'] = [];
        $variables['date'] = new DateTime();
        $variables['tenantRepresentative'] = '';
        $variables['patientRepresentative'] = '';

        if ($this->stylesTemplate) {
            $zip->addFromString('styles.xml', $this->templateEngine->render($this->stylesTemplate, $variables));
        }
        
        if ($this->contentTemplate) {
            $zip->addFromString('content.xml', $this->templateEngine->render($this->contentTemplate, $variables));
        }
        
        $zip->close();
    }
    
    public function getDocumentPDFPath()
    {
        return dirname($this->documentPath) .'/'. basename($this->documentPath, 'odt') .'pdf';
    }
    
    /**
     * getter for documentPath
     * 
     * @return mixed return value for 
     */
    public function getDocumentPath()
    {
        return $this->documentPath;
    }
    
    /**
     * setter for templateEngine
     *
     * @param mixed 
     * @return self
     */
    public function setTemplateEngine($value)
    {
        $this->templateEngine = $value;
        return $this;
    }
    
    /**
     * getter for templateEngine
     * 
     * @return mixed return value for 
     */
    public function getTemplateEngine()
    {
        return $this->templateEngine;
    }
    
    /**
     * setter for contentTemplate
     *
     * @param mixed 
     * @return self
     */
    public function setContentTemplate($value)
    {
        $this->contentTemplate = $value;
        return $this;
    }
    
    /**
     * getter for contentTemplate
     * 
     * @return mixed return value for 
     */
    public function getContentTemplate()
    {
        return $this->contentTemplate;
    }
    
    /**
     * setter for stylesTemplate
     *
     * @param mixed 
     * @return self
     */
    public function setStylesTemplate($value)
    {
        $this->stylesTemplate = $value;
        return $this;
    }
    
    /**
     * getter for stylesTemplate
     * 
     * @return mixed return value for 
     */
    public function getStylesTemplate()
    {
        return $this->stylesTemplate;
    }
}