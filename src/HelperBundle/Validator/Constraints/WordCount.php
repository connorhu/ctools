<?php

namespace CT\HelperBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class WordCount extends Constraint
{
    public $min;
    
    public $max;
    
    public $message = 'The text of the field contains more than %max% words.';
    
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}