<?php

namespace CT\CMSBundle\Controller;

use Psr\Container\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response,
    Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Doctrine\ORM\Tools\Pagination\Paginator,
    Doctrine\ORM\Mapping\MappingException;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\AbstractFOSRestController,
    FOS\RestBundle\Controller\Annotations\Get;

use FOS\RestBundle\Context\Context;

use CT\CMSBundle\Entity\Interfaces\VisibilityInterface;
use CT\CMSBundle\Entity\Interfaces\OrderableInterface,
    CT\CMSBundle\Entity\Interfaces\GrouppedOrderableInterface;

use App\EntityFilter\FieldFinder\FieldFinder,
    App\EntityFilter\Services\FilterFormBuilder,
    App\EntityFilter\Helpers\QueryBuilderHelper;
use Symfony\Component\HttpFoundation\RequestStack;

class BaseController extends AbstractController implements RequestAwareInterfase
{
    use Traits\MergeEntities;
    use Traits\ControllerTrait;
    
    const PAGE_TYPE_CGET = 'cget';
    const PAGE_TYPE_GET = 'get';
    const PAGE_TYPE_NEW = 'new';
    const PAGE_TYPE_EDIT = 'edit';
    
    const MODIFICATION_EVENT_POST = 'put';
    const MODIFICATION_EVENT_PUT = 'post';
    
    public Request $request;
    protected array $documentNames;
    
    protected string $bundleName = 'App';
    protected string $entityManagerName = 'default';

    protected bool $filter = false;
    protected bool $export = false;
    protected bool $import = false;
    protected bool $createDocument = false;
    protected bool $simpleFilter = true;
    protected bool $merge = false;
    protected bool $newDocumentEnabled = true;
    
    protected string $templateSubdir = '';

    protected string $cmsRoutePrefix = '';

    protected string $documentName;
    protected string $documentClass;
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->createDocument = method_exists($this, 'newAction');

        $this->documentNames = $this->createDocumentNames($this->documentName);

        $this->requestStack = $requestStack;
        $request = $requestStack->getMainRequest();

        if ($request !== null) {
            $this->request = $request;
        }
    }

    protected function templateData($type): array
    {
        $templateData = [
            'documentNames' => $this->createDocumentNames($this->documentName),
            'document_names' => $this->createDocumentNames($this->documentName),
            'cms_route_prefix' => $this->cmsRoutePrefix,
            'edit' => $this->isEdit($this->request, $this->cmsRoutePrefix),
            'view' => $this->isView($this->request, $this->cmsRoutePrefix),
        ];

        if ($type === self::PAGE_TYPE_CGET) {
            $templateData['exportEnabled'] = $this->export;
            if ($this->export || $this->import) {
                $templateData['documentClass'] = $this->documentClass;
                $templateData['document_class'] = $this->documentClass;
            }

            $templateData['allowImport'] = $this->import;
            $templateData['import_enabled'] = $this->import;

            $templateData['create_document'] = $this->createDocument;
            $templateData['simple_filter'] = $this->simpleFilter;
            $templateData['merge_enabled'] = $this->merge;
        }
        
        return $templateData;
    }

    protected function isFormat($format): bool
    {
        $routeParams = $this->request->attributes->get('_route_params');
        return isset($routeParams['_format']) && $routeParams['_format'] === $format;
    }

    /**
     *  Doctrine Methods
     */
    protected function getManager()
    {
        return $this->getDoctrine()->getManager($this->entityManagerName);
    }
    
    protected function getRepository($documentClass = false, $entityManagerName = false)
    {
        $thisDocumentClass = $documentClass === false ? $this->documentClass : $documentClass;
        $thisEntityManager = $entityManagerName === false ? $this->entityManagerName : $entityManagerName;
        return $this->getDoctrine()->getRepository($thisDocumentClass, $thisEntityManager);
    }

    protected function autoRedirect($event, $entity = null)
    {
        return $this->routeRedirectView($this->cmsRoutePrefix.'get_'. $this->documentNames['plural']);
    }
    
    protected function getContextWithGroups($groups)
    {
        $context = new Context();
        $context->setGroups($groups);
        return $context;
    }

    /**
     *  Handle Methods
     */

    /**
     * View
     */
    protected function handleGet($document)
    {
        $form = $this->getForm($document, $this->getFormOptions());
        
        $templateData = $this->templateData('get');
        
        $data = [
            'document' => $document,
            'form' => $form->createView(),
        ];

        return $this->render($this->templateSubdir .'/'. $this->documentNames['camel'] .'/view.html.twig', $data + ['form' => $templateData]);
    }
    
    /**
     *  Listing
     */
    protected int $limit = 100;
    protected array $limits = array(
        10 => 10,
        20 => 20,
        50 => 50,
        100 => 100,
        200 => 200,
        -1 => 'All',
    );
    
    private function getListStateSessionKey(): string
    {
        return $this->templateSubdir .'list_state_'. str_replace('\\', '_', $this->documentClass);
    }
    
    private function getListStateDefaults(): array
    {
        return [
            'offset' => 0,
            'limit' => $this->limit,
            'search' => false,
        ];
    }
    
    protected function clearListState(): void
    {
        $this->request->getSession()->set($this->getListStateSessionKey(), $this->getListStateDefaults());
    }
    
    private function saveListState($variables): void
    {
        $this->request->getSession()->set($this->getListStateSessionKey(), $variables);
    }

    private function restoreListState(): array
    {
        if (!$this->request->getSession()->has($this->getListStateSessionKey())) {
            return $this->getListStateDefaults();
        }
        
        return $this->request->getSession()->get($this->getListStateSessionKey());
    }

    protected function handleCget(): Response
    {
        $restored = $this->restoreListState();
        
        $offset = (int) $this->request->query->get('offset', $restored['offset']);
        $limit = (int) $this->request->query->get('limit', $restored['limit']);
        $search = $this->request->query->get('q', $restored['search']);

        if ($search === '') {
            $this->request->query->remove('q');
        }
        elseif (!$this->request->query->has('q') && $restored['search']) {
            $this->request->query->set('q', $restored['search']);
        }

        $qb = $this->queryBuilder();

        if ($this->filter) {
            $filterForm = $this->getFilterForm($this->documentClass);
            $filterForm->handleRequest($this->request);

            $filterQueryBuilder = new QueryBuilderHelper();
            $filterQueryBuilder->setEntityManager($this->getDoctrine()->getManager());
            $filterQueryBuilder->setQueryBuilder($qb);
            $filterQueryBuilder->setFilterForm($filterForm);
            $filterQueryBuilder->addFilters();
        }

        $this->orderingList($qb);
        $this->queryFilter($qb, $search !== '' ? $search : false);

        $qb->setFirstResult($offset);
        if ($limit > 0) {
            $qb->setMaxResults($limit);
        }

        $paginator = new Paginator($qb->getQuery(), $fetchJoinCollection = true);
        $numberOfRows = count($paginator);
        
        $this->saveListState(['search' => $search, 'offset' => $offset, 'limit' => $limit]);
        
        $data = array(
            'offset' => $offset,
            'total' => $numberOfRows,
            'limit' => $limit,
            'limits' => $this->limits,
            'pages' => $limit > 0 ? ceil($numberOfRows / $limit) : 1,
            'page' => $offset / $limit + 1,
            'search' => $search,

            'rows' => $this->getResult($qb),
        );
        
        $templateData = $this->templateData('cget');
        $templateData['has_new'] = method_exists($this, 'newAction');
        
        if ($this->filter) {
            $templateData['filter_form'] = $filterForm->createView();
        }
        
        $contextVariables = $data + $templateData;

        return $this->render($this->templateSubdir .'/'. $this->documentNames['camel'] .'/list.html.twig', $contextVariables);
    }
    
    protected function orderingList($queryBuilder)
    {
        $queryBuilder->addOrderBy('e.id', 'desc');
    }
    
    protected function queryFilter($queryBuilder, $search = false)
    {
    }
    
    protected function getResult($queryBuilder)
    {
        return $queryBuilder->getQuery()->getResult();
    }

    protected function queryBuilder()
    {
        return $this->getRepository()->createQueryBuilder('e');
    }

    protected function filterFields($fieldFinder)
    {
        $fieldFinder->addAll();
    }
    
    protected function getFilterForm($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $finder = FieldFinder::in($this->documentClass, $em);
        $this->filterFields($finder);

        $formFactory = $this->container->get('form.factory');
        $ffb = new FilterFormBuilder($formFactory);
        $ffb->setManager($em);
        $callbacks = [
            'transform' => [$this, 'transformFilterFormValues'],
            'reverseTransform' => [$this, 'reverseTransformFilterFormValues'],
        ];
        
        return $ffb->getFiterForm($entity, $finder->getFields(), $callbacks);
    }
    
    public function transformFilterFormValues($data)
    {
        return $data;
    }
    
    public function reverseTransformFilterFormValues($data)
    {
        return $data;
    }
    
    protected function newDocumentInstance()
    {
        throw new \Exception('implement newDocumentInstance method');
    }
    
    protected function newFormTypeInstance()
    {
        throw new \Exception('implement newFormTypeInstance method');
    }

    protected function getFormOptions(): array
    {
        if ($this->isEdit($this->request, $this->cmsRoutePrefix)) {
            $method = $this->request->getMethod() === 'PATCH' ? 'PATCH' : 'PUT';
            $action = $this->generateUrl($this->cmsRoutePrefix.'put_'. $this->documentNames['singular'], [
                'event' => $this->request->attributes->get('event'),
                'document' => $this->request->attributes->get('document')->getId()
            ]);
        } else {
            $method = 'POST';
            $action = $this->generateUrl($this->cmsRoutePrefix.'post_'. $this->documentNames['singular'], [
                'event' => $this->request->attributes->get('event')
            ]);
        }
        
        $options = [
            'method' => $method,
            'action' => $action,
        ];
        
        if ($this->isFormat('json')) {
            $options['csrf_protection'] = false;
        }
        
        return $options;
    }

    protected function handleNewAction(): Response
    {
        return $this->handleNew();
    }

    protected function handleNew(): Response
    {
        $form = $this->getForm($this->newDocumentInstance(), $this->getFormOptions());

        $templateData = $this->templateData('new');

        $templateName = $this->templateSubdir.'/'.$this->documentNames['camel'].'/edit.html.twig';
        $data = ['form' => $form->createView()];
        return $this->render($templateName, $data + $templateData);
    }

    protected function prePersist($document, $form)
    {
    }

    protected function handlePostAction($document)
    {
        return $this->handlePost($document);
    }

    protected function handlePost($document)
    {
        $form = $this->getForm($document);
        $form->handleRequest($this->request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $this->prePersist($document, $form);
            $em->persist($document);
            $em->flush();

            if (!$this->isFormat('json')) {
                if ($this->request->headers->has('x-no-redirection')) {
                    return new JsonResponse([
                        'document' => $document->getId(),
                    ]);
                }

                return $this->autoRedirect('post', $document);
            }

            return $this->view($document);
        }

        $templateData = $this->templateData('new');

        $templateName = $this->templateSubdir.'/'.$this->documentNames['camel'].'/edit.html.twig';
        $data = ['form' => $form->createView()];
        return $this->render($templateName, $data + $templateData);
    }

    protected function handleEditAction($document)
    {
        return $this->handleEdit($document);
    }

    protected function handleEdit($document)
    {
        $data = [];
        $data['form'] = $this->getForm($document, $this->getFormOptions())->createView();

        $templateData = $this->templateData('edit');

        $templateName = $this->templateSubdir.'/'.$this->documentNames['camel'].'/edit.html.twig';
        return $this->render($templateName, $data + $templateData);
    }
    
    protected function getTemplateDataForEditDocumentPage()
    {
        return [
            'document' => $this->documentNames,
            'edit' => $this->isEdit($this->request)
        ];
    }
    
    protected function preUpdate($document, $form)
    {
    }

    protected function handlePutAction($document)
    {
        return $this->handlePut($document);
    }

    protected function handlePut($document)
    {
        $form = $this->getForm($document);
        $form->handleRequest($this->request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $this->preUpdate($document, $form);
            $em->flush();
            
            if (!$this->isFormat('json')) {
                $view = $this->autoRedirect('put', $document);
            }
            else {
                $view = $this->view()->setData($document);
            }
            
            return $view;
        }

        $data = [];
        $data['form'] = $form;
        
        $templateData = $this->templateData('edit');

        return $this->view($data, 200)
            ->setTemplate($this->templateSubdir .'/'. $this->documentNames['camel'] .'/edit.html.twig')
            ->setTemplateVar('form')
            ->setTemplateData($templateData);
    }
    
    
    public function getForm(object $object = null, array $options = null): FormInterface
    {
        return $this->createForm($this->formTypeClass(), $object, $options !== null ? $options : $this->getFormOptions());
    }
    
    protected function getItem($documentId)
    {
        return $this->getDocument($documentId);
    }
    protected function getDocument($documentId, $documentName = false, $findMethod = 'find')
    {
        return $this->getDoctrine()->getRepository($documentName === false ? $this->documentClass : $documentName)->$findMethod($documentId);
    }
    protected function getDocuments($documentIds, $documentName = false)
    {
        return $this->getDoctrine()->getRepository($documentName === false ? $this->documentClass : $documentName)->createQueryBuilder('e')
            ->select('e')
            ->where('e.id IN (:ids)')
            ->setParameter('ids', $documentIds)
            ->getQuery()
            ->getResult();
    }

    protected function createNamedBuilder($name, $type = 'form', $data = null, $options = array())
    {
        return $this->get('form.factory')->createNamedBuilder($name, $type, $data, $options);
    }

    public function handlePublic($document)
    {
        if ($document instanceof VisibilityInterface) {
            $document->setPublic($document::VISIBILITY_PUBLIC);
        }

        $this->getDoctrine()->getManager()->flush();
        return $this->autoRedirect('public');
    }

    public function handlePrivate($document)
    {
        if ($document instanceof VisibilityInterface) {
            $document->setPublic($document::VISIBILITY_PRIVATE);
        }
        
        $this->getDoctrine()->getManager()->flush();
        return $this->autoRedirect('private');
    }
    
    private function getAllObjects($document)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('d')
            ->select('partial d.{id, recordOrder}')
            ->addOrderBy('d.recordOrder')
            ->addOrderBy('d.id');
        
        if ($document instanceof GrouppedOrderableInterface) {
            $groupName = $document::getOrderingGroupName();
            $groupObject = $document->{'get'. ucfirst($groupName)}();
            $queryBuilder->andWhere('d.'. $groupName .' = :group_object')->setParameter('group_object', $groupObject);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    protected function handleOrdering($document, $after, $group = null)
    {
        if (!($document instanceof OrderableInterface)) return;
        
        $em = $this->getDoctrine()->getManager();
        
        if ($after === null) {
            $document->setRecordOrderEmpty();
        }
        elseif (0 === (int) $after) {
            $document->setRecordOrderFirst();
        }
        else {
            $insertAfterObject = $this->getDocument($after);

            if ($insertAfterObject === null) {
                throw $this->createNotFoundException();
            }
            
            $document->setRecordOrderAfter($insertAfterObject);
        }
        
        if ($document instanceof GrouppedOrderableInterface) {
            $groupFieldname = $document->getOrderingGroupName();
            $classMetadata = $em->getClassMetadata(get_class($document));
            
            try {
                $groupFieldMetadata = $classMetadata->getAssociationMapping($groupFieldname);
                
                if ($group !== null) {
                    $group = $this->getDocument($group, $groupFieldMetadata['targetEntity']);
                }
            }
            catch (MappingException $e) {
            }
            
            if ($document->{'get'. ucfirst($groupFieldname)}() != $group) {
                // valtozott a csoport
                $document->{'set'. ucfirst($groupFieldname)}($group);
            }
        }

        $em->flush($document);

        return $this->autoRedirect('ordering');
    }

    protected function handleDeleteAction($document)
    {
        return $this->handleDelete($document);
    }
    
    /**
     * Torlest vegzo methodus
     */
    protected function handleDelete($document)
    {
        try {
            $this->testDelete($document);
        }
        catch (\Exception $e) {
            $response = ['error' => $e->getMessage()];
            if ($this->request->isXmlHttpRequest()) {
                return new JsonResponse($response, 500);
            }

            $this->addFlash('error', $response['error']);

            return $this->autoRedirect('delete');
        }
        
        $em = $this->getDoctrine()->getManager();
        // TODO van-e olyan mezo ahol ra hivatkoznak?

        if ($document instanceof OrderableInterface) {
            $document->setRecordOrderEmpty();
            $em->flush($document);
        }

        $em->remove($document);
        $em->flush();

        if ($this->request->isXmlHttpRequest()) {
            return new JsonResponse(true);
        }
        else {
            return $this->autoRedirect('delete');
        }
    }
    
    protected function testDelete($document)
    {
        return true;
    }
}
