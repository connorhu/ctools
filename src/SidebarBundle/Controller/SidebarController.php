<?php

namespace CT\SidebarBundle\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Twig\Environment;

class SidebarController extends AbstractController
{
    public function __construct(
        private readonly array $config
    )
    {
    }

    private function getItems($config): array
    {
        $authorizationChecker = $this->getAuthorizationChecker();
        $tmp = [];
        foreach ($config as $sidebarItem) {
            $accessGranted = true;

            if (isset($sidebarItem['roles'])) {
                foreach ($sidebarItem['roles'] as $role) {
                    $accessGranted &= $authorizationChecker->isGranted($role);
                }

                unset($sidebarItem['roles']);
            }


            if (!$accessGranted) {
                continue;
            }

            $buffer = $sidebarItem;
            $activeChild = false;

            if (isset($sidebarItem['childs']) && count($sidebarItem['childs'])) {
                $buffer['childs'] = $this->getItems($sidebarItem['childs']);
                
                foreach ($buffer['childs'] as $child) {
                    $activeChild |= $child['active'];
                }
            }

            $buffer['active'] = (isset($sidebarItem['controller']) && $sidebarItem['controller'] === $this->masterRequestRouteMatch['controller']) || $activeChild;
            
            $tmp[] = $buffer;
        }

        return $tmp;
    }

	public function getSidebarData(): array
    {
		return $this->getItems($this->config);
	}

    /**
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    private function buildControllerList(): array
    {
        $router = $this->container->get('router');
        
        $routesInControllers = [];
        
        foreach ($router->getRouteCollection()->all() as $name => $route) {
            $controller = $route->getDefaults();
            $controller = $controller['_controller']; // TODO: osszevonni 5.3 miatt lett szeteszedve
            
            if (!str_contains($controller, '::')) {
                continue;
            }

            list($controllerFullName, $method) = explode('::', $controller);
            $controllerName = substr($controllerFullName, strrpos($controllerFullName, '\\')+1);
            
            $routesInControllers[$controllerName][] = $name;
        }
        
        return $routesInControllers;
    }
    
    private function getController($requestAttributes): string
    {
        $controllerString = $requestAttributes->get('_controller');
        $controllerString = substr($controllerString, 0, strpos($controllerString, ':'));

        return substr($controllerString, strrpos($controllerString, '\\')+1);
    }
    
    private array $masterRequestRouteMatch = ['controller' => null, 'route' => null];

    public function sidebar(Request $request): Response
    {
        $masterRequest = $this->getRequestStack()->getMainRequest();
        $this->masterRequestRouteMatch['controller'] = $this->getController($masterRequest->attributes);
        $this->masterRequestRouteMatch['route'] = $masterRequest->attributes->get('_route');
        // $this->controllers = $this->buildControllerList();

    	$sidebarData = $this->getSidebarData();

    	if ($this->getTwig()->getLoader()->exists('sidebar.html.twig')) {
    		$templateName = 'sidebar.html.twig';
    	}
    	else {
    		$templateName = '@Sidebar/sidebar.html.twig';
    	}

   		return $this->render($templateName, [
            'sidebar' => $sidebarData,
        ]);
    }

    public function renderView(string $view, array $parameters = []): string
    {
        return $this->container->get('twig')->render($view, $parameters);
    }

    private function getRequestStack(): RequestStack
    {
        return $this->container->get('request_stack');
    }

    private function getTwig(): Environment
    {
        return $this->container->get('twig');
    }

    private function getAuthorizationChecker(): AuthorizationChecker
    {
        return $this->container->get('security.authorization_checker');
    }
}
