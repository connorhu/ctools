<?php

namespace CT\CMSBundle\Entity\Traits;

use \DateTime;

use Doctrine\ORM\Mapping as ORM;

trait TimestampableTrait
{
    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    protected $updatedAt;
    
    public function updateFieldUpdatedAt()
    {
        $this->updatedAt = new DateTime();
    }

    public function updateFieldCreatedAt()
    {
        $this->createdAt = new DateTime();
    }
    
    /**
     * getter for createdAt
     * 
     * @return mixed return value for 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * getter for updatedAt
     * 
     * @return mixed return value for 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}