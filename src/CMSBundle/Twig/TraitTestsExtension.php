<?php

namespace CT\CMSBundle\Twig;

use CT\CMSBundle\Entity\Interfaces\VisibilityInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class TraitTestsExtension extends AbstractExtension
{
    public function getTests(): array
    {
        return array(
            new TwigTest('visibility', function ($object) { return $object instanceof VisibilityInterface; }),
            new TwigTest('orderable', function ($object) { return $object instanceof VisibilityInterface; }),
        );
    }

    public function getName(): string
    {
        return 'trait_tests';
    }
}