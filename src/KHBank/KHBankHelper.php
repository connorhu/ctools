<?php

namespace CT\KHBank;

use App\Entity\Transaction;

class KHBankHelper
{
    const PAYMENT_PURCHASE_TYPE = 'PU';
    const PAYMENT_REFUND_TYPE = 'RE';
    
    const CURRENCY_HUF = 'HUF';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';

    const LANGUAGE_CODE_HU = 'HU';
    const LANGUAGE_CODE_DE = 'DE';
    const LANGUAGE_CODE_ES = 'ES';
    const LANGUAGE_CODE_EN = 'EN';
    const LANGUAGE_CODE_FI = 'FI';
    const LANGUAGE_CODE_FR = 'FR';
    const LANGUAGE_CODE_IT = 'IT';
    const LANGUAGE_CODE_NL = 'NL';
    const LANGUAGE_CODE_NO = 'NO';
    const LANGUAGE_CODE_PL = 'PL';
    const LANGUAGE_CODE_PT = 'PT';
    const LANGUAGE_CODE_RO = 'RO';
    const LANGUAGE_CODE_SK = 'SK';
    const LANGUAGE_CODE_SV = 'SV';
    
    protected $privateKeyPath;
    protected $isTest;
    
    public function __construct($privateKeyPath, $mid, $isTest = false)
    {
        $this->privateKeyPath = $privateKeyPath;
        $this->mid = $mid;
        $this->isTest = $isTest;
        $this->privateKey = openssl_get_privatekey($privateKeyPath);
    }
    
    public function __destruct()
    {
        // openssl_free_key($this->privateKey);
    }
    
    private function sign($values)
    {
        $buffer = '';
        foreach (['mid', 'txid', 'type', 'amount', 'ccy'] as $key)
        {
            $buffer .= '&'. $key .'='. $values[$key];
        }

        $buffer = ltrim($buffer, '&');
        
        $fp = fopen($this->privateKeyPath, "r");
        $priv_key_content = fread($fp, 8192);
        fclose($fp);

        openssl_sign($buffer, $signature, $priv_key_content);

        return bin2hex($signature);
    }
    
    protected function getHost()
    {
        if ($this->isTest) {
            return 'https://pay.sandbox.khpos.hu/pay/v1';
        }

        return 'https://pay.khpos.hu/pay/v1';
    }
    
    public function transactionUrl($transactionId, $amount, $ccy = self::CURRENCY_HUF, $code = self::LANGUAGE_CODE_HU)
    {
        $host = $this->getHost();
        $args = [
            'mid' => $this->mid,
            'txid' => $transactionId,
            'type' => self::PAYMENT_PURCHASE_TYPE,
            'amount' => (int) ($amount * 100),
            'ccy' => $ccy,
        ];
        
        $args['sign'] = $this->sign($args);

        $langArgument = '&lang='. $code;
        return $host .'/PGPayment?'. http_build_query($args) . ($code != self::LANGUAGE_CODE_HU ? $langArgument : '');
    }

    public function refundUrl($transactionId, $amount, $ccy = self::CURRENCY_HUF)
    {
        $host = $this->getHost();
        $args = [
            'mid' => $this->mid,
            'txid' => $transactionId,
            'type' => self::PAYMENT_REFUND_TYPE,
            'amount' => (int) ($amount * 100),
            'ccy' => $ccy,
        ];
        
        $args['sign'] = $this->sign($args);

        return $host .'/PGPayment?'. http_build_query($args);
    }
    
    public function fetchPage($url)
    {
        return trim(file_get_contents($url));
    }
    
    public function statusUrl($transactionId)
    {
        $host = $this->getHost();
        return $host .'/PGResult?mid='. $this->mid .'&txid='. $transactionId;
    }
    
    public function getStatus($transactionId)
    {
        $content = $this->fetchPage($this->statusUrl($transactionId));
        $lines = explode("\n", $content);
        $lines = array_map('trim', $lines);
        return $lines;
    }
    
    public function updateStatusWithTransaction(Transaction $transaction)
    {
        $transactionId = $transaction->getId();
        
        if ($transaction->getType() === Transaction::TRANSACTION_REFUND_TYPE) {
            return;
        }
        
        $statusData = $this->getStatus($transaction->getId());
        $statusString = $statusData[0];
        $status = Transaction::bankStringToStatus($statusString);
        
        if ($status === Transaction::TRANSACTION_STATUS_REFOUNDED || $status === Transaction::TRANSACTION_STATUS_REFOUND_PENDING) {
            $refund = $transaction->getRefund();
            $refund->setStatus($status);
            
            if ($status === Transaction::TRANSACTION_STATUS_REFOUNDED) {
                $refund->setBankLicenceNumber($statusData[3]);
            }
        }
        else {
            $transaction->setStatus($status);

            if ($status === Transaction::TRANSACTION_STATUS_ACKNOWLEDGED) {
                $transaction->setBankLicenceNumber($statusData[3]);
            }
        }

        return $status;
    }

    public function refundTransaction(Transaction $refundTransaction, Transaction $refundedTransaction)
    {
        $refundUrl = $this->refundUrl($refundedTransaction->getId(), $refundTransaction->getPrice(), $refundedTransaction->getCurrencyISOCode());
        $content = $this->fetchPage($refundUrl);
        
        for ($i = 0; $i < 5; ++$i) {
            $status = $this->updateStatusWithTransaction($refundedTransaction);

            switch ($status) {
                case Transaction::TRANSACTION_STATUS_PENDING:
                case Transaction::TRANSACTION_STATUS_REFOUND_PENDING:
                    sleep(1);
                    continue 2;
            }

            break;
        }
    }
}