<?php

namespace CT\SimpleMailerBundle;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

use CT\SimpleMailerBundle\DependencyInjection\SimpleMailerExtension;

class CTSimpleMailerBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new SimpleMailerExtension();
    }
}
