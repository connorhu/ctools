<?php

namespace CT\SidebarBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SidebarExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
    	if (isset($configs[0])) {
    		$container->setParameter('sidebar_config', $configs[0]);
    	}
    	else {
    		$container->setParameter('sidebar_config', []);
    	}

        $loader = new PhpFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('service.php');
    }
}