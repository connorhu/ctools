<?php

namespace CT\CMSBundle\Form\Extensions;

use Symfony\Component\Form\AbstractTypeExtension;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class CollectionTypeExtension extends AbstractTypeExtension
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'add_item_button_text' => 'Add new'
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['add_item_button_text'] = $options['add_item_button_text'];
    }
    
    public static function getExtendedTypes(): iterable
    {
        return [CollectionType::class];
    }
}