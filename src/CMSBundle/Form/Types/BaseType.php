<?php

namespace CT\CMSBundle\Form\Types;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\String\Inflector\EnglishInflector;

class BaseType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getDocumentName(): string
    {
        $inflector = new EnglishInflector();

        $name = get_class($this);
        $lastStart = strrpos($name, '\\')+1;
        $stringToPluralize = strtolower(substr($name, $lastStart, strlen($name) - $lastStart - 4));

        return $inflector->pluralize($stringToPluralize)[0];
    }
}