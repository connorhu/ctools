<?php

namespace CT\HelperBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class EditorController extends Controller
{
    /**
     * @Route("/fileOpen", name="_editor_file_open")
     */
    public function openInEditorAction()
    {
        if ($this->container->getParameter('editor')) {
            exec($this->container->getParameter('editor') .'"'. $this->getRequest()->query->get('path') .'"');
        }
        exit;
    }
}
