<?php

namespace ConnorToolsBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
// use Symfony\Component\PropertyAccess\PropertyAccess;
// use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class CollectionTypeExtension extends AbstractTypeExtension
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'add_item_button_text' => 'Add new'
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['add_item_button_text'] = $options['add_item_button_text'];
    }
    
    public function getExtendedType(): string
    {
        return 'collection';
    }
}
