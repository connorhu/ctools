<?php

namespace CT\CMSBundle\Form\Extensions;

use Symfony\Component\Form\AbstractTypeExtension,
    Symfony\Component\OptionsResolver\OptionsResolver,
    Symfony\Component\Form\FormView,
    Symfony\Component\Form\FormInterface,
    Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EntityTypeExtension extends AbstractTypeExtension
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'add_item_button_text' => 'Add new',
            'add_item_url' => false
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['add_item_button_text'] = $options['add_item_button_text'];
        $view->vars['add_item_url'] = $options['add_item_url'];
    }

    public static function getExtendedTypes(): iterable
    {
        return [EntityType::class];
    }
}