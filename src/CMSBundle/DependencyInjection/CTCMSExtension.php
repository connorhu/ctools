<?php

namespace CT\CMSBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CTCMSExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $this->registerFormTemplates($container);
    }
    
    private function registerFormTemplates($container)
    {
        $resources = [];

        if ($container->hasParameter('twig.form.resources')) {
            $resources = $container->getParameter('twig.form.resources');
        }

        $resources[] = '@CTCMS/Form/ct_forms.html.twig';

        $container->setParameter('twig.form.resources', $resources);
    }
}