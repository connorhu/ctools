<?php

namespace CT\SimpleMailerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class SimpleMailerExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $config = $this->processConfiguration(new Configuration(), $configs);
        
        // from
        if (isset($config['from']['email'])) {
            $from = [$config['from']['email'] => $config['from']['name']];
            $container->setParameter('simple_mailer.from', $from);
        }
        
        // templates
        $templates = [];
        if (isset($config['templates'])) {
            foreach ($config['templates'] as $name => $value) {
                $templates[$name] = $value['template'];
            }
        }
        $container->setParameter('simple_mailer.templates', $templates);
    }
    
    public function getAlias(): string
    {
        return 'simple_mailer';
    }
}