<?php

namespace CT\IEBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class Transformer
{
    public $className;
    
    public function __construct($className)
    {
        $this->className = $className;
    }
}