<?php

namespace CT\HelperBundle\Barcode;

interface BarcodeInterface
{
    public function getBarcode();
}