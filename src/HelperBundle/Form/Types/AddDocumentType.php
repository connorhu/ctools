<?php

namespace CT\HelperBundle\Form\Types;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class AddDocumentType extends DocumentType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'add_document_button_text' => 'Add new',
            'add_document_url' => false
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['add_document_button_text'] = $options['add_document_button_text'];
        $view->vars['add_document_url'] = $options['add_document_url'];
    }

    public function getName()
    {
        return 'add_document';
    }
    
    public function getParent()
    {
        return 'document';
    }
}
